#!/bin/sh
#
# ------------------------------
# -----  Config start ----------
#


# Lucks
#leandro.baldisssera@gmail.com

LinuxIso="debian" # Choose debian or ubuntu
Swap="nao" # Choose how much '2gb', '4gb', '8gb', '16gb', '24gb', '48gb' if you don't want to put 'nao'
iptables="nao" # If you want to use iptables (media protection) put 'sim' otherwise put 'nao'
pula="nao" # Putting yes you will go directly to the configuration of iptables

#
# ------ Config end -------------
# -------------------------------
#
if test "$pula" = "nao"; then

if test "$LinuxIso" != "debian" && test "$LinuxIso" != "ubuntu"; then
	echo "INVALID DISTRO!"
	return
 
fi



# Swap php
if test "$Swap" = "2gb"; then
 dd if=/dev/zero of=/arquivoswap count=2048 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap


elif test "$Swap" = "4gb"; then
 dd if=/dev/zero of=/arquivoswap count=4096 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap

elif test "$Swap" = "8gb"; then
 dd if=/dev/zero of=/arquivoswap count=8192 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap

elif test "$Swap" = "16gb"; then
 dd if=/dev/zero of=/arquivoswap count=16384 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap

elif test "$Swap" = "24gb"; then
 dd if=/dev/zero of=/arquivoswap count=24576 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap


elif test "$Swap" = "48gb"; then
 dd if=/dev/zero of=/arquivoswap count=49152 bs=1M
 ls -lah /arquivoswap
 chmod 777 /arquivoswap
 mkswap /arquivoswap
 swapon /arquivoswap



elif test "$Swap" = "nao"; then
echo "->Não será usado o Swap"
fi



# extras
 apt install unzip
 apt-get install zip
 sudo apt install protobuf-compiler
#instalação Para anti rollback
 apt-get install gdb
 apt install screen



# instalação php
if test "$LinuxIso" = "debian"; then
 apt-get -y install libboost-all-dev php-xml php-gd php-curl php-mbstring php-mbstring php-gettext php-xml  php mysql-server libapache2-mod-auth-mysql php-mysql 
elif test "$LinuxIso" = "ubuntu"; then
 apt-get -y install libboost-all-dev php-xml php-gd php-curl php-mbstring php-mbstring php-gettext php-xml  php mysql-server libapache2-mod-auth-mysql php-mysql 
fi
 
#instalação c++ stuff
if test "$LinuxIso" = "debian"; then
 apt-get install git cmake build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-iostreams-dev libcrypto++-dev liblua5.2-dev libluajit-5.1-dev libpugixml-dev libgmp3-dev libspdlog-dev
 apt-get install libboost-all-dev
 /etc/init.d/apache2 reload

 
elif test "$LinuxIso" = "ubuntu"; then
  apt-get install git cmake build-essential libboost-dev libboost-system-dev libboost-filesystem-dev libboost-iostreams-dev libcrypto++-dev liblua5.2-dev libluajit-5.1-dev libmysqlclient-dev libpugixml-dev libgmp3-dev libspdlog-dev
 /etc/init.d/apache2 reload
sudo apt install protobuf-compiler
 
fi
  

#instalação mysql 
 apt-get -y install mysql-server 
echo "


->Digite na ordem abaixo!/ -> Type in order below!"
echo "->use mysql"
 echo "->update user set plugin='' where User='root';"
 echo "->flush privileges;
->exit"
 mysql -u root

#
 
 
 
 echo "


->Digite na ordem abaixo!/ -> Type in order below!"
 echo "->aperte enter -> hit enter"
 echo "->yes"
 echo "->Coloque a nova senha do banco de dados ->Enter the new database password;
->yes
->yes
->yes"
 mysql_secure_installation
 
  apt-get -y install libmariadb-dev-compat
 
#
#instalação  phpmyadmin
#
 apt-get -y install phpmyadmin
 ln -s /usr/share/phpmyadmin /var/www/html
#
  /etc/init.d/apache2 reload
#

#instalação apache2
  apt-get -y install apache2

#Permissão nos arquivos
 chmod -R 777 /var/www/html

 apt-get update &&  apt-get dist-upgrade

elif test "$Swap" = "sim"; then
echo "->Direto para iptables"
fi

# iptables 
if test "$iptables" = "sim"; then
echo "
______ ___________ _____ _    _  ___  _     _ 
|  ___|_   _| ___ |  ___| |  | |/ _ \| |   | |
| |_    | | | |_/ | |__ | |  | / /_\ | |   | |
|  _|   | | |    /|  __|| |/\| |  _  | |   | |
| |    _| |_| |\ \| |___\  /\  | | | | |___| |____
\_|    \___/\_| \_\____/ \/  \/\_| |_\_____\_____/"

# CLEAR RULES

 iptables -t filter -F
 iptables -t filter -X


#loop-back (localhost) ok
 iptables -t filter -A INPUT -i lo -j ACCEPT
 iptables -t filter -A OUTPUT -o lo -j ACCEPT

#SSH ACCEPT
 iptables -t filter -A INPUT -p tcp --dport 22 -j ACCEPT
 iptables -t filter -A OUTPUT -p tcp --dport 22 -j ACCEPT
  
# Active Localhost OPENVPN SECURITY
 iptables -t filter -A INPUT -p tcp --dport 22 -s 100:100:100:100 -j ACCEPT
  
#HTTPS
 iptables -t filter -A INPUT -p tcp --dport 443 -j ACCEPT
 iptables -t filter -A OUTPUT -p tcp --dport 443 -j ACCEPT
 iptables -t filter -A INPUT -p udp --dport 443 -j ACCEPT
 iptables -t filter -A OUTPUT -p udp --dport 443 -j ACCEPT

 
# ANTI DDOS  ok

 iptables -A FORWARD -p tcp --syn -m limit --limit 1/second -j ACCEPT
 iptables -A FORWARD -p udp -m limit --limit 1/second -j ACCEPT
 iptables -A FORWARD -p icmp --icmp-type echo-request -m limit --limit 1/second -j ACCEPT
 iptables -A FORWARD -p tcp --tcp-flags SYN,ACK,FIN,RST RST -m limit --limit 1/s -j ACCEPT
 iptables -A INPUT -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT


# Reject spoofed packets ok

 iptables -A INPUT -s 10.0.0.0/8 -j DROP
 iptables -A INPUT -s 169.254.0.0/16 -j DROP
 iptables -A INPUT -s 172.16.0.0/12 -j DROP
 iptables -A INPUT -s 127.0.0.0/8 -j DROP
 iptables -A INPUT -s 224.0.0.0/4 -j DROP
 iptables -A INPUT -d 224.0.0.0/4 -j DROP
 iptables -A INPUT -s 240.0.0.0/5 -j DROP
 iptables -A INPUT -d 240.0.0.0/5 -j DROP
 iptables -A INPUT -s 0.0.0.0/8 -j DROP
 iptables -A INPUT -d 0.0.0.0/8 -j DROP
 iptables -A INPUT -d 239.255.255.0/24 -j DROP
 iptables -A INPUT -d 255.255.255.255 -j DROP

# Cloudflare + Sucuri 
 iptables -A INPUT -s 192.88.134.0/23 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 185.93.228.0/22 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 66.248.200.0/22 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 208.109.0.0/22 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 2a02:fe80::/29 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 204.93.240.0/24 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 204.93.177.0/24 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 199.27.128.0/21 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 173.245.48.0/20 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 103.22.200.0/22 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 141.101.64.0/18 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 108.162.192.0/18 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -s 190.93.240.0/20 -p tcp --dport https -j ACCEPT
 iptables -A INPUT -p tcp --dport https -j DROP


# Drop excessive RST packets to avoid smurf attacks ok
 iptables -A INPUT -p tcp -m tcp --tcp-flags RST RST -m limit --limit 2/second --limit-burst 2 -j ACCEPT

# Anyone who tried to portscan us is locked out for an entire day.

 iptables -A INPUT   -m recent --name portscan --rcheck --seconds 86400 -j DROP
 iptables -A FORWARD -m recent --name portscan --rcheck --seconds 86400 -j DROP

# Once the day has passed, remove them from the portscan list

 iptables -A INPUT   -m recent --name portscan --remove
 iptables -A FORWARD -m recent --name portscan --remove

# These rules add scanners to the portscan list, and log the attempt.

 iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
 iptables -A INPUT   -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP
 iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j LOG --log-prefix "Portscan:"
 iptables -A FORWARD -p tcp -m tcp --dport 139 -m recent --name portscan --set -j DROP

# Drop all invalid packets ok

 iptables -A INPUT -m state --state INVALID -j DROP
 iptables -A FORWARD -m state --state INVALID -j DROP
 iptables -A OUTPUT -m state --state INVALID -j DROP

# Conf 2

 iptables -A INPUT -p tcp --syn -m limit --limit 2/s --limit-burst 30 -j ACCEPT
 iptables -A INPUT -p icmp --icmp-type echo-request -m limit --limit 1/s -j ACCEPT
 iptables -A INPUT -p tcp --tcp-flags ALL NONE -m limit --limit 1/h -j ACCEPT
 iptables -A INPUT -p tcp --tcp-flags ALL ALL -m limit --limit 1/h -j ACCEPT

 
# Account

 iptables -t filter -A OUTPUT -p udp --dport 587 -j ACCEPT
 iptables -t filter -A INPUT -p udp --dport 587 -j ACCEPT
 iptables -t filter -A OUTPUT -p udp --dport 587 -j ACCEPT
 iptables -t filter -A INPUT -p udp --dport 587 -j ACCEPT

  # Limit connections on Tibia Ports
 iptables -A INPUT -p tcp -m recent --rcheck --seconds 60 -j REJECT
 iptables -A INPUT -p tcp --dport 7171 -m connlimit --connlimit-above 10 -m recent --set -j REJECT
 iptables -A INPUT -p tcp --dport 7172 -m connlimit --connlimit-above 10 -m recent --set -j REJECT

# Allow connections from origin
 iptables -I INPUT -m state --state ESTABLISHED,RELATED -j ACCEPT

 # MYSQL

 iptables -t filter -A INPUT -p tcp --dport 3306 -j ACCEPT
 iptables -t filter -A INPUT -p udp --dport 3306 -j ACCEPT
 
# Allow SSH (PUTTY)
 iptables -I INPUT -i $if_ext -p tcp --dport 22 -j ACCEPT

# Limit connections
 iptables -I INPUT -p tcp -m state --state NEW,ESTABLISHED -m recent --set -j ACCEPT
 iptables -I INPUT -p tcp -m state --state NEW -m recent --update --seconds 3 --hitcount 20 -j DROP

# Block TCP-CONNECT scan attempts (SYN bit packets)
#iptables -A INPUT -p tcp --syn -j DROP

# Block TCP-SYN scan attempts (only SYN bit packets)
 iptables -A INPUT -m conntrack --ctstate NEW -p tcp --tcp-flags SYN,RST,ACK,FIN,URG,PSH SYN -j DROP

# Block TCP-FIN scan attempts (only FIN bit packets)
 iptables -A INPUT -m conntrack --ctstate NEW -p tcp --tcp-flags SYN,RST,ACK,FIN,URG,PSH FIN -j DROP

# Block TCP-ACK scan attempts (only ACK bit packets)
 iptables -A INPUT -m conntrack --ctstate NEW -p tcp --tcp-flags SYN,RST,ACK,FIN,URG,PSH ACK -j DROP

# Block TCP-NULL scan attempts (packets without flag)
 iptables -A INPUT -m conntrack --ctstate INVALID -p tcp --tcp-flags ! SYN,RST,ACK,FIN,URG,PSH SYN,RST,ACK,FIN,URG,PSH -j DROP

#Block "Christmas Tree" TCP-XMAS scan attempts (packets with FIN, URG, PSH bits)
 iptables -A INPUT -m conntrack --ctstate NEW -p tcp --tcp-flags SYN,RST,ACK,FIN,URG,PSH FIN,URG,PSH -j DROP

# Block DOS - Teardrop
 iptables -A INPUT -p UDP -f -j DROP

# Block DDOS - Smurf
 iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP
 iptables -A INPUT -p ICMP --icmp-type echo-request -m pkttype --pkttype broadcast -j DROP
 iptables -A INPUT -p ICMP --icmp-type echo-request -m limit --limit 3/s -j ACCEPT

# Block DDOS - SYN-flood
 iptables -A INPUT -p TCP --syn -m iplimit --iplimit-above 9 -j DROP

# Block DDOS - SMBnuke
 iptables -A INPUT -p UDP --dport 135:139 -j DROP
 iptables -A INPUT -p TCP --dport 135:139 -j DROP

# Block DDOS - Jolt
 iptables -A INPUT -p ICMP -f -j DROP

# Block DDOS - Fraggle
 iptables -A INPUT -p UDP -m pkttype --pkt-type broadcast -j DROP
 iptables -A INPUT -p UDP -m limit --limit 3/s -j ACCEPT

# Creates logs of the rest of the connections
 iptables -A INPUT -i $if_ext -p all -j LOG --log-prefix " - FIREWALL: droped -> "




elif test "$iptables" = "nao"; then
echo "->Não será feito iptables"
fi
 

  
echo "Instação Completa
Lucks~~

"
